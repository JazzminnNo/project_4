﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="passwordreset.aspx.cs" Inherits="WebApplication1.passwordreset" %>

<!DOCTYPE html>
    <body>
         <div style="position: absolute;top: 0;bottom: 0;right: 0;left: 0;text-align: center;overflow: hidden;white-space: nowrap;">
            <div style="position: relative;display: inline-block;height: 100%;vertical-align: middle;"></div>
                <div style="position: relative;display: inline-block;vertical-align: middle;">
                    <form id="form1" runat="server">
                    <div>
                        <h1><%= msg %></h1>
                        <asp:Panel ID="Pane_image" runat="server" Visible="true">
                        <%--here you can set image according to your requirement--%>
                        <img src="images.jpg" alt="" />
                        </asp:Panel>
                        <asp:Panel ID="Panel_reset_pwd" runat="server" Visible="false">
            
            
                            <table class="style1">
                                <tr>
                                    <td class="style2">
                                        Enter Your New Password:</td>
                                    <td>
                                        <asp:TextBox ID="txt_pwd" runat="server" TextMode="Password"></asp:TextBox>
                                        
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txt_pwd" ErrorMessage="Reqiired"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat=server display=dynamic   ControlToValidate="txt_pwd" 
                                            ErrorMessage="Password must be 6-12 nonblank characters." 
                                            ValidationExpression="[^\s]{6,12}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        Retype Password</td>
                                    <td>
                                        <asp:TextBox ID="txt_retype_pwd" runat="server" TextMode="Password"></asp:TextBox>
                                         
                                    </td>
                                    <td>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txt_retype_pwd" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server"
                                            ControlToCompare="txt_pwd" ControlToValidate="txt_retype_pwd"
                                            ErrorMessage="Enter Same Password"></asp:CompareValidator>   
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        &nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_change_pwd" runat="server" onclick="btn_change_pwd_Click"
                                            Text="Change Password" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                       </td>
                                    <td>
                                        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
       
                        </asp:Panel>
                    </div>
                    </form>
            </div>
           </div>    
    </body>
</html>
