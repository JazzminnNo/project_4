﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class qandaccountactivation : System.Web.UI.Page
    {
        public string code = "";
        public string option = "";
        public string msg;
        public DataTable data;
        public DataRow row;
        public string pessoa;



        SqlConnection sqlCon = new SqlConnection("Data Source=145.24.222.167,8080; Database=project4App;Persist Security Info=True; User ID=sa; Password=Estrela0120");


        protected void Page_Load(object sender, EventArgs e)
        {
            this.code = Request.Params["code"];
            this.option = Request.Params["option"];


            if (code != "" && option != "")
            {
                AccountValidate(); 
            }
        }

        public Boolean Connect()
        {
            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        public Boolean Disconnect()
        {
            try
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        public void AccountValidate()
        {
            data = GetAccount();

            if (data.Rows.Count == 1)
            {
                if (data.Rows[0][2].ToString() == "0")
                {
                    try
                    {
                        string sqlString = "Update account_activation set account_status = 1, activation_date =@date where activation_code = @code;";
                        SqlCommand sqlStatement = new SqlCommand(sqlString, sqlCon);

                        DateTime date = DateTime.Now;

                        sqlStatement.Parameters.AddWithValue("@date", date);
                        sqlStatement.Parameters.AddWithValue("@code", code);

                        sqlCon.Open();
                        sqlStatement.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        msg = "There was an exception in method SaveQuestion: " + ex.ToString();
                    }
                    finally
                    {
                        sqlCon.Close();
                    }
                    msg = "Je account is geactiveerd!";
                }
                else if (data.Rows[0][2].ToString() == "1") { msg = "Uw account is al geactiveerd Deze link is niet langer in gebruik."; }
            }
            else
            {
                msg = "Je account kan niet worden geactiveerd! Neem contact met het team op.";
            }

        }

        public DataTable GetAccount()
        {
            string sql = "select * from account_activation where activation_code = '" + code + "'";
            DataTable data1 = GetDataFromDB(sql);
            return data1;
        }

        public DataTable GetDataFromDB(String sqlString)
        {
            DataTable dataCollection = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, sqlCon);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);
            this.Connect();
            data.Fill(dataCollection);
            this.Disconnect();

            return dataCollection;
        }

    }
}