﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Este código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace WebApplication1 {
    
    
    public partial class passwordreset {
        
        /// <summary>
        /// Controle form1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Controle Pane_image.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Pane_image;
        
        /// <summary>
        /// Controle Panel_reset_pwd.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel_reset_pwd;
        
        /// <summary>
        /// Controle txt_pwd.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txt_pwd;
        
        /// <summary>
        /// Controle RequiredFieldValidator1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
        
        /// <summary>
        /// Controle txt_retype_pwd.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txt_retype_pwd;
        
        /// <summary>
        /// Controle RequiredFieldValidator2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
        
        /// <summary>
        /// Controle CompareValidator1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator CompareValidator1;
        
        /// <summary>
        /// Controle btn_change_pwd.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn_change_pwd;
        
        /// <summary>
        /// Controle lbl_msg.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lbl_msg;
    }
}
