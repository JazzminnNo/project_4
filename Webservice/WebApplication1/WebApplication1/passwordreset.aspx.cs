﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class passwordreset : System.Web.UI.Page
    {
        public string code = "";
        public string option = "";        
        public string msg;
        public DataTable data;
        public DataRow row;        
        public string password = "";
        public string user;


        SqlConnection sqlCon = new SqlConnection("Data Source=145.24.222.167,8080; Database=project4App;Persist Security Info=True; User ID=sa; Password=Estrela0120");


        protected void Page_Load(object sender, EventArgs e)
        { 
            this.code = Request.Params["code"];
            this.option = Request.Params["option"];

            if (this.option == "resetpassword")
            {
                string dt = this.GetAccount();

                if (dt != null)
                {
                    user = dt;
                    Panel_reset_pwd.Visible = true;
                }
                else
                {
                    this.msg = "Deze code bestaat niet...";
                }  
            }
        }

        public Boolean Connect()
        {
            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        public Boolean Disconnect()
        {
            try
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }      

        public string GetAccount()
        {
            DataTable data1 = new DataTable();           
            try
            {
                string sql = "select * from pasword_reset where code = '" + this.code + "'";
                data1 = GetDataFromDB(sql);
                if (data1.Rows.Count != 0)
                {
                    return data1.Rows[0][0].ToString();
                }
                
            }
            catch (Exception ex)
            {
                msg = "There was an exception in method SaveQuestion: " + ex.ToString();
            }

            return null;
        }

        public DataTable GetDataFromDB(String sqlString)
        {
            DataTable dataCollection = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, sqlCon);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);
            this.Connect();
            data.Fill(dataCollection);
            this.Disconnect();

            return dataCollection;
        }

        public static string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                string b64 = Convert.ToBase64String(sha.Hash);
                b64 = b64.Replace('+', '-');
                return b64.Replace('/', '_');
            }
        }

        protected void btn_change_pwd_Click(object sender, EventArgs e)
        {
            
            password = txt_pwd.Text;
            ChangePassword();
            msg = "Je nieuwe password is met sucess gecreeerd.";
        }

        public void ChangePassword()
        {
            try
            {
                string sqlString = "Update users set pass = @pass where user_number = @user_n;";
                string sqlString1 = "Delete from pasword_reset where user_number = @user1_n;";
                SqlCommand sqlStatement = new SqlCommand(sqlString, sqlCon);
                SqlCommand sqlStatement1 = new SqlCommand(sqlString1, sqlCon);
                sqlStatement.Parameters.AddWithValue("@pass", HashPassword(password));
                sqlStatement.Parameters.AddWithValue("@user_n", user);
                sqlStatement1.Parameters.AddWithValue("@user1_n", user);

                sqlCon.Open();
                sqlStatement.ExecuteNonQuery();
                sqlStatement1.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                msg = "There was an exception in method SaveQuestion: " + ex.ToString();
            }
            finally
            {
                sqlCon.Close();
            }
        }
    }
}