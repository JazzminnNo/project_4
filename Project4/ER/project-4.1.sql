CREATE TABLE teacher_list {
    teacher_id CHAR(10),
    teacher_email CHAR(50),
    PRIMARY KEY (teacher_id)
};

CREATE TABLE account_activation {
    user_id CHAR(10),
    activation_code CHAR(500),
    account_status BOOL,
    activation_date DATE,
    PRIMARY KEY (user_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
};

CREATE TABLE account_authentication {
    user_id CHAR(10),
    token CHAR(500),
    last_active CHAR(500),
    PRIMARY KEY (user_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
};

INSERT INTO teacher_list (teacher_id, teacher_email) VALUES (
    PEIAC, a.c.van.rooij-peiman@hr.nl,
    ROODA, d.a.van.roon@hr.nl,
    MAGGG, g.maggiore@hr.nl,
    VRAJL, j.l.m.vrancken@hr.nl,
    ABBAM, m.abbadi@hr.nl,
    AMIGA, a.amighi@hr.nl,
);