CREATE TABLE category (
    category_id INT,
    name CHAR(10),
    description TEXT(300),
    PRIMARY KEY (category_id)
);

CREATE TABLE rank (
    rank_id INT,
    name CHAR(10),
    description CHAR(200),
    minimum_points INT,
    PRIMARY KEY (rank_id)
);

CREATE TABLE users (
    user_id CHAR(10),
    name CHAR(30),
    surname CHAR(50),
    email CHAR(50),
    pass CHAR(50),
    type ENUM('Student', 'Teacher', 'Peercoach'),
    PRIMARY KEY (user_id),
    office CHAR(20)
);

CREATE TABLE user_has_rank (
    user_id CHAR(10),
    rank_id INT,
    points INT,
    PRIMARY KEY (user_id,rank_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (rank_id) REFERENCES rank(rank_id)
);

CREATE TABLE question (
    question_id INT,
    category_id INT,
    user_id CHAR(10),
    title CHAR(50),
    question_text TEXT(600),
    post_date DATE,
    PRIMARY KEY (question_id,category_id,user_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (category_id) REFERENCES category(category_id)
);

CREATE TABLE answer (
    answer_id INT,
    question_id INT,
    category_id INT,
    user_id CHAR(10),
    answer_text TEXT(500),
    post_date DATE,
    PRIMARY KEY (answer_id,question_id,category_id,user_id),
    FOREIGN KEY (category_id) REFERENCES question(category_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (question_id) REFERENCES question(question_id)
);

CREATE TABLE users_like (
    user_id CHAR(10),
    answer_id INT,
    question_id INT,
    category_id INT,
    PRIMARY KEY (user_id,answer_id,question_id,category_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (answer_id) REFERENCES answer(answer_id),
    FOREIGN KEY (question_id) REFERENCES question(question_id),
    FOREIGN KEY (category_id) REFERENCES answer(category_id)
);