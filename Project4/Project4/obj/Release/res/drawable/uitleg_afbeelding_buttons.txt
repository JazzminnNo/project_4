﻿Plaats je afbeelding in deze folder (drawable) en ga dan binnen het gewenste .axml bestand naar het element
(een button bijvoorbeeld) waar je deze wilt gebruiken. Klik op dat element (of ga naar source en type het erbij) 
en gebruik de property Drawable x (waarbij x kan staan voor Left, Right, etc...). Drawable zet de afbeelding op positie
x in het element (bijvoorbeeld links of rechts).

BELANGRIJK:
Als je een verwijzing maakt naar je bestand in de 'drawable' folder, zet dan NIET
de extensie van de afbeelding erbij, anders kan het niet worden gevonden en krijg je errors.