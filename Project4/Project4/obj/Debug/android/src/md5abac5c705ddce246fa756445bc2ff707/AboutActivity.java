package md5abac5c705ddce246fa756445bc2ff707;


public class AboutActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("RaysHotDogs.AboutActivity, Project4", AboutActivity.class, __md_methods);
	}


	public AboutActivity ()
	{
		super ();
		if (getClass () == AboutActivity.class)
			mono.android.TypeManager.Activate ("RaysHotDogs.AboutActivity, Project4", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
