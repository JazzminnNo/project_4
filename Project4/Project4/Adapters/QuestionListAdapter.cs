﻿using Android.App;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace Project4.Adapters
{
    public class QuestionListAdapter: BaseAdapter<Question>
    {
        List<Question> items;
        Activity context;

        public QuestionListAdapter(Activity context, List<Question> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Question this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.home_questions_row, null);
            }

            convertView.FindViewById<TextView>(Resource.Id.homeTitleQuestion).Text = item.title;
            convertView.FindViewById<TextView>(Resource.Id.homeUserIDQuestion).Text = item.user;
            convertView.FindViewById<TextView>(Resource.Id.homeUsertypeQuestion).Text = item.user_number;
            convertView.FindViewById<TextView>(Resource.Id.homeDescriptionQuestion).Text = item.question_text;
            convertView.FindViewById<TextView>(Resource.Id.homeCategoryQuestion).Text = item.category;
            convertView.FindViewById<TextView>(Resource.Id.homePostDateQuestion).Text = item.date;
            Console.WriteLine("FROM QUESTIONLISTADAPTER: ITEM.USERNUMBER: " + item.user_number);
            Console.WriteLine("FROM QUESTIONLISTADAPTER: ITEM.USER: " + item.user);
            if (item.question_text.Length > 100) 
            {
                convertView.FindViewById<TextView>(Resource.Id.homeReadMoreQuestion).Text = "Lees meer >";
                convertView.FindViewById<TextView>(Resource.Id.homeDescriptionQuestion).Text = item.question_text.Substring(0, 100) + "...";
            }

            if (item.user_number.Length == 5)
            {
                convertView.FindViewById<TextView>(Resource.Id.homeUsertypeQuestion).Text = "(leraar)";
                convertView.FindViewById<TextView>(Resource.Id.homeUsertypeQuestion).SetTextColor(Color.ParseColor("#00FF00"));
            }
            else 
            {
                convertView.FindViewById<TextView>(Resource.Id.homeUsertypeQuestion).Text = "(student)";
                convertView.FindViewById<TextView>(Resource.Id.homeUsertypeQuestion).SetTextColor(Color.ParseColor("#0000FF"));
            }

            return convertView;
        }
    }
}