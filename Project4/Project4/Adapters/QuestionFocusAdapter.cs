﻿using Android.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

namespace Project4.Adapters
{
    public class QuestionFocusAdapter : BaseAdapter<Question>
    {
        List<Question> items;
        Activity context;

        public QuestionFocusAdapter(Activity context, List<Question> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Question this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.question_focus_row, null);
            }

            convertView.FindViewById<TextView>(Resource.Id.qaTitleQuestion).Text = item.title;
            convertView.FindViewById<TextView>(Resource.Id.qaFullNameQuestion).Text = "Door: " + item.user;
            convertView.FindViewById<TextView>(Resource.Id.qaDescriptionQuestion).Text = item.text;
            convertView.FindViewById<TextView>(Resource.Id.qaCategory).Text = item.category;
            
            return convertView;
        }
    }
}