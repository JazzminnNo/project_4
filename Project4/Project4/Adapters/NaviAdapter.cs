﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Data;

namespace Project4.Adapters
{
    public class NaviAdapter: BaseAdapter<string>
    {
        private List<string> items;
        Activity context;
        string UserNumber;
        DataTable dt = new DataTable();

        public NaviAdapter(Activity context, List<string> items, string usernumber)
        {
            this.context = context;
            this.items = items;
            UserNumber = usernumber;
            Console.WriteLine("VANUIT NAAAVIA ADAPTER DIT ISSS usernumbeer" + UserNumber);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override string this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];


            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Menu.menu, null);
            }
            var navi_button = convertView.FindViewById<TextView>(Resource.Id.nav_item);
             convertView.FindViewById<TextView>(Resource.Id.nav_item).Text = item;

            if (!navi_button.HasOnClickListeners)
            {
                navi_button.Click += (sender, e) =>
                {
                    int x = position;
                    if(item == "Profiel")
                        btnProfielClick(position, item);
                    else
                    {
                        btnUitlogClick(position, item);
                    }

                };
            }
            return convertView;
        }

        private void btnUitlogClick(int position, string item)
        {
            Toast.MakeText(this.context, "U bent uitgelogd", ToastLength.Short).Show();
            var intent = new Intent(context, typeof(MainActivity));
            context.StartActivity(intent);
            context.Finish();
        }

            private void btnProfielClick(int position, string item)
        {
            var intent = new Intent(context, typeof(ProfielPagina));
            string usanum = context.Intent.Extras.GetString("usernumber");
            intent.PutExtra("userNumber", usanum);
            context.StartActivity(intent);
            context.Finish();
        }
    }
}