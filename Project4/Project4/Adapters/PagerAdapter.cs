﻿using Android.Content;
using Android.Support.V4.App;
using Java.Lang;
using Project4.Resources.Fragments;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Project4.Adapters
{
    public class MyAdapter : FragmentPagerAdapter
    {
        //List<Fragment> fragments = null;
        int PAGE_COUNT = 7;
        private string[] tabTitles = new string[] { "Home", "Development", "Analyse", "Project", "Skills", "SLC", "Overig" };
        private Context context;

        public MyAdapter(FragmentManager fm, Context context) : base(fm)
        {
            this.context = context;
        }

        public override Fragment GetItem(int position)
        {
            Fragment selected_fragment= new Fragment();
            if(position == 0)
            {
                selected_fragment = home_frag2.NewInstance(position + 1);
            }
            if (position == 1)
            {
                selected_fragment = dev_frag.NewInstance(position + 1);
            }
            if (position == 2)
            {
                selected_fragment = ana_frag.NewInstance(position + 1);
            }
            if (position == 3)
            {
                selected_fragment = project_frag.NewInstance(position + 1);
            }
            if (position == 4)
            {
                selected_fragment = skills_frag.NewInstance(position + 1);
            }
            if (position == 5)
            {
                selected_fragment = slc_frag.NewInstance(position + 1);
            }
            if (position == 6)
            {
                selected_fragment = overig_frag.NewInstance(position + 1);
            }
            return selected_fragment;
        }

        public override int Count
        {
            get
            {
                return PAGE_COUNT;
            }
        }
        public override ICharSequence GetPageTitleFormatted(int position)
        {
            ICharSequence cs;
            if (position == 0)
                cs = new Java.Lang.String(tabTitles[position]);
            else if (position == 1)
                cs = new Java.Lang.String(tabTitles[position]);
            else if (position == 2)
                cs = new Java.Lang.String(tabTitles[position]);
            else if (position == 3)
                cs = new Java.Lang.String(tabTitles[position]);
            else if (position == 4)
                cs = new Java.Lang.String(tabTitles[position]);
            else if (position == 5)
                cs = new Java.Lang.String(tabTitles[position]);
            else
                cs = new Java.Lang.String(tabTitles[position]);
            return cs;   
        }
    }
}