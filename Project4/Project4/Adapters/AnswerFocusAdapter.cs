﻿using Android.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Data;

namespace Project4.Adapters
{
    class AnswerFocusAdapter : BaseAdapter<Answer>
    {
        List<Answer> items;
        List<Answer> tempo_list;
        Activity context;

        DataTable dt = new DataTable();
        AnswerLike al = new AnswerLike();

        DataTable aantal_likes = new DataTable();

        private string userNumber;
        private string questionId;
        string like_holder;

        public AnswerFocusAdapter(Activity context, List<Answer> items, string usernumber, int questionid)
        {
            this.context = context;
            this.items = items;
            userNumber = usernumber;
            questionId = questionid.ToString();
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override Answer this[int position]
        {
            get { return items[position]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            Console.WriteLine("FROM ANSWERFOCUSADAPTER: item position: " + item.answer_id);

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.answer_focus_row, null);
            }

            convertView.FindViewById<TextView>(Resource.Id.qaFullNameAnswer).Text = "Door: " + item.user_name;
            convertView.FindViewById<TextView>(Resource.Id.qaUserNumberAnswer).Text = item.user_number;
            convertView.FindViewById<TextView>(Resource.Id.qaDescriptionAnswer).Text = item.answer_text;
            convertView.FindViewById<TextView>(Resource.Id.qaPostDateAnswer).Text = item.post_date;
            
            convertView.FindViewById<ImageButton>(Resource.Id.btnLike);
            convertView.FindViewById<TextView>(Resource.Id.amountOfLikes);                
                aantal_likes = al.getLike(item.answer_id);
                string amountOflike = "0";
                foreach(DataRow row in aantal_likes.Rows)
                {
                    amountOflike = "";
                    amountOflike = amountOflike + row[0].ToString();
                }


                convertView.FindViewById<TextView>(Resource.Id.amountOfLikes).Text = amountOflike;
                like_holder = convertView.FindViewById<TextView>(Resource.Id.amountOfLikes).Text;

            convertView.FindViewById<TextView>(Resource.Id.answeridTxt).Text = item.answer_id.ToString();

            if (!convertView.FindViewById<ImageButton>(Resource.Id.btnLike).HasOnClickListeners)
                {
                convertView.FindViewById<ImageButton>(Resource.Id.btnLike).Click += (sender, e) =>
                    {
                        
                        btnOneClick(convertView.FindViewById<TextView>(Resource.Id.answeridTxt).Text);
                        
                    };
                }

            return convertView;
        }

        private void btnOneClick(string answer_id)
        {
            dt = al.searchLike(userNumber, answer_id);
            Console.WriteLine("ANSWER_IDDDD  SHOW ME NOW btnOneClick" + answer_id);

            if (dt.Rows.Count > 0)
            {
                Console.WriteLine("Laat me zieeeeeen wat hier faaackin in zit!!!!!!!!!!!!!!! WAAAROM GAA IE NAAR DEZE CONDITIE WAT IS HIERIN: " + dt.Rows[0][0] + " " + dt.Rows[0][1] + " " + dt.Rows[0][2]);
                Toast.MakeText(context, "Je hebt dit antwoord AL geliket", ToastLength.Short).Show();
            }
            else
            {
                AnswerLike answerlike = new AnswerLike();
                answerlike.user_number = userNumber;
                answerlike.answer_id = answer_id;
                answerlike.question_id = questionId;
                answerlike.save();

                Toast.MakeText(context, "Je hebt dit antwoord geliket", ToastLength.Short).Show();
                context.Recreate();
            }
        }       
    }
}