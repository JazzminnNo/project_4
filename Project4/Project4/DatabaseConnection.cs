﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace Project4
{
    class DatabaseConnection
    {
        private SqlConnection connection;
        private String ServerIp = "145.24.222.167";
        private String serverPort = "8080";
        private String DBUserID = "sa";
        private String DBPassword = "Estrela0120";
        private String DBName = "project4App";
        private String connString;

        public DatabaseConnection()
        {
            this.connString = ("Data Source=" + this.ServerIp + "," + this.serverPort + "; Database=" + this.DBName + ";Persist Security Info=True; User ID=" + this.DBUserID + "; Password=" + this.DBPassword);
            this.connection = new SqlConnection(connString);
        }

        public Boolean Connect()
        {
            try
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    this.connection.Open();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        public Boolean Disconnect()
        {
            try
            {
                if (this.connection.State == ConnectionState.Open)
                {
                    this.connection.Close();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        public DataTable GetDataFromDB(String sqlString)
        {
            DataTable dataCollection = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(dataCollection);
            this.Disconnect();

            return dataCollection;
        }

        public void InsertIntoDB()
        {
            string sqlString = "INSERT INTO category (name, description) VALUES ('Analyse', 'Beste vak ooit')";
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

            this.Connect();
            Console.WriteLine("A connection is made: ");
            sqlStatement.ExecuteNonQuery();
            this.Disconnect();
        }
        //return errors list
        //1 - user exists
        //0 - user register sucess
        //-1 - Database error
        public DataTable getUserbyId(string usernumber)
        {
            string sqlString = "select user_number, name, surname, email, type, office from users where user_number = @user_number";
            Console.WriteLine("USSSSSERNUMBEEERR PROFIEEEEL:" + usernumber);
            DataTable selected_user = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@user_number", usernumber);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(selected_user);
            this.Disconnect();

            return selected_user;
        }
        public int UserRegistration(User user)
        {
            if (UserVerify(user.user_number))
            {          
                return 1;
            }
            try
            {
                string sqlString = "INSERT INTO users (user_number, name, surname, email, pass, type) VALUES (@user_number, @name, @surname, @email, @pass, @type)";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                DateTime date1 = DateTime.Now; 
                string activation_code = HashPassword(user.email+date1.ToString());

                string sqlString1 = "INSERT INTO account_activation (user_id,activation_code,account_status,activation_date) VALUES (@user_number,@activation_code,@status,@date)";
                SqlCommand sqlStatement1 = new SqlCommand(sqlString1, this.connection);

                Console.WriteLine("Date: "+date1+ " activation code: "+ activation_code);

                //check if user is a teacher
                DataTable dataCollection2 = new DataTable();
                string sqlString2 = "Select count(*) From teacher where teacher_code = @id1;";
                SqlCommand sqlStatement2 = new SqlCommand(sqlString2, this.connection);
                sqlStatement2.Parameters.AddWithValue("@id1", user.user_number);
                SqlDataAdapter data2 = new SqlDataAdapter(sqlStatement2);                
                this.Connect();
                data2.Fill(dataCollection2);
                this.Disconnect();

                //Console.WriteLine("Ik ben een docent "+ dataCollection2.Rows[0][0]);                


                sqlStatement1.Parameters.AddWithValue("@user_number", user.user_number);
                sqlStatement1.Parameters.AddWithValue("@activation_code", activation_code);
                sqlStatement1.Parameters.AddWithValue("@status", 0);
                sqlStatement1.Parameters.AddWithValue("@date", date1);

                sqlStatement.Parameters.AddWithValue("@user_number", user.user_number);
                sqlStatement.Parameters.AddWithValue("@name", user.name);
                sqlStatement.Parameters.AddWithValue("@surname", user.surname);
                sqlStatement.Parameters.AddWithValue("@email", user.email);
                sqlStatement.Parameters.AddWithValue("@pass", HashPassword(user.password));


                if ((int)dataCollection2.Rows[0][0] == 1)
                {                    
                    sqlStatement.Parameters.AddWithValue("@type", "Leraar");
                }
                else
                {
                    sqlStatement.Parameters.AddWithValue("@type", "student");                    
                }                

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();
                sqlStatement1.ExecuteNonQuery();
                SendMail mail = new SendMail();  
                mail.sendActivationmail(activation_code,user.email);
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method UserRegistration: "+ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }
            return 0;
        }

        public DataTable UserLogin(string email, string password) {

            //Console.WriteLine("email: "+email+"password: "+password);
            string sqlString= "select u.user_number, u.name, u.surname, u.email, u.type, u.office, a.account_status, a.activation_code From users u, account_activation a where u.email = @email and u.pass = @pass and u.user_number = a.user_id";

            DataTable dataCollection = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@email", email);
            sqlStatement.Parameters.AddWithValue("@pass", HashPassword(password));
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);
            
            this.Connect();
            data.Fill(dataCollection);
            this.Disconnect();
            
            return dataCollection;
        }

        public  string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                string b64 = Convert.ToBase64String(sha.Hash);
                b64 = b64.Replace('+', '-');
                return b64.Replace('/', '_');
            }
        }

        public bool UserVerify(string user_id)
        {
            DataTable dataCollection = new DataTable();
            string sqlString = "Select count(*) From users where user_number = @id;";
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@id", user_id);                     
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(dataCollection);
            this.Disconnect();

            string amountRows = "";

            foreach(DataRow row in dataCollection.Rows)
            {
                amountRows = amountRows + row[0];              
            }
                    
            if (Int32.Parse(amountRows) > 0)
            {
                return true;
            }  
            return false;
        }

        public DataTable getAllQuestion(bool nieuwfilter)
        {
            DataTable questions = new DataTable();

            questions = this.GetDataFromDB("Select q.question_id, q.title, q.question_text, c.name, u.name, u.surname, q.post_date, q.user_number from question q, category c, users u where c.category_id = q.category_id and u.user_number = q.user_number order by q.post_date DESC; ");

            if (!nieuwfilter) {
                questions = this.GetDataFromDB("SELECT q.question_id, q.title, q.question_text, c.name, u.name, u.surname, q.post_date, q.user_number FROM category c, users u,  (SELECT COUNT(u.user_number) as like_count, q.question_id FROM question q, users u, users_like_question uq WHERE u.user_number = uq.user_number and q.question_id = uq.question_id "
                                         + "GROUP BY q.question_id) as likes_per_question RIGHT JOIN question q ON q.question_id = likes_per_question.question_id WHERE c.category_id = q.category_id and u.user_number = q.user_number ORDER BY likes_per_question.like_count DESC;");
            }
            
            return questions;
        }

        public DataTable getAllQuestionsFromCategory(int category_id, bool nieuwfilter)
        {
            string sqlString = "Select q.question_id, q.title, q.question_text, c.name, u.name, u.surname, q.post_date, q.user_number from question q, category c, users u where c.category_id = q.category_id and u.user_number = q.user_number and q.category_id = @category_id order by q.post_date DESC;";

            if (!nieuwfilter)
            {
                sqlString = "SELECT q.question_id, q.title, q.question_text, c.name, u.name, u.surname, q.post_date, q.user_number FROM category c, users u,  (SELECT COUNT(u.user_number) as like_count, q.question_id FROM question q, users u, users_like_question uq WHERE u.user_number = uq.user_number and q.question_id = uq.question_id "
                                         + "GROUP BY q.question_id) as likes_per_question RIGHT JOIN question q ON q.question_id = likes_per_question.question_id WHERE c.category_id = q.category_id and u.user_number = q.user_number and q.category_id = @category_id ORDER BY likes_per_question.like_count DESC;";
            }

            DataTable questions = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@category_id", category_id);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(questions);
            this.Disconnect();

            return questions;
        }

        public DataTable getAllQuestionsFromSpecificUser(int user_number)
        {
            string sqlString = "select * from question where user_number = @user_number;";

            DataTable user_questions = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@user_number", user_number);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(user_questions);
            this.Disconnect();

            return user_questions;
        }

        public DataTable getSelectedQuestion(int question_id)
        {
            string sqlString = "Select q.question_id, q.title, q.question_text, c.name, u.name, u.surname, q.post_date, q.user_number from question q, category c, users u where c.category_id = q.category_id and u.user_number = q.user_number and q.question_id = @question_id;";

            DataTable selected_question = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@question_id", question_id);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(selected_question);
            this.Disconnect();

            return selected_question;
        }

        public int SaveQuestion(Question question)
        {
            try
            {
                string sqlString = "INSERT INTO question (category_id, user_number, title, question_text, post_date) VALUES (@category_id, @user_number, @title, @question_text, @post_date)";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                DateTime date1 = DateTime.Now;

                Console.WriteLine("DIT IS DE NAAAM VAN DE CATEGORYY:" + question.category);
                switch (question.category)
                {
                    case "Development": question.category_id = 1;break;
                    case "Analyse": question.category_id = 2; break;
                    case "Project": question.category_id = 3; break;
                    case "Skills": question.category_id = 4; break;
                    case "SLC": question.category_id = 5; break;
                    case "Overig": question.category_id = 6; break;
                }
                Console.WriteLine("DIT IS DE ID VAN DE CATEGORYY:"+ question.category_id);
                sqlStatement.Parameters.AddWithValue("@category_id", question.category_id);
                sqlStatement.Parameters.AddWithValue("@user_number", question.user_number);
                sqlStatement.Parameters.AddWithValue("@title", question.title);
                sqlStatement.Parameters.AddWithValue("@question_text", question.question_text);
                sqlStatement.Parameters.AddWithValue("@post_date", date1);              

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();                
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method SaveQuestion: " + ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }
            return 1;
        }

        public int SaveLike(QuestionLike question_like)
        {
            try
            {
                string sqlString = "INSERT INTO users_like_question (user_number, question_id) VALUES (@user_number,  @question_id)";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                sqlStatement.Parameters.AddWithValue("@user_number", question_like.user_number);
                sqlStatement.Parameters.AddWithValue("@question_id", question_like.question_id);

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method SaveQuestionLike: " + ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }
            return 1;
        }

        public DataTable searchQuestionLike(string usernumber, string question_id)
        {
            string sqlString = "Select q.question_id, u.user_number from question q, users u, users_like_question uq where u.user_number = uq.user_number and q.question_id = uq.question_id and uq.user_number = @actieve_user and uq.question_id = @question_id";

            DataTable likes = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@actieve_user", usernumber);
            sqlStatement.Parameters.AddWithValue("@question_id", question_id);

            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(likes);
            this.Disconnect();

            return likes;
        }

        public DataTable getAmountOfLikeofEachQuestion(int question_id)
        {
            string sqlString = "Select COUNT(u.user_number) as like_count from question q, users u, users_like_question uq where u.user_number = uq.user_number and q.question_id = uq.question_id and uq.question_id = @question_id GROUP BY q.question_id;";

            DataTable count = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@question_id", question_id);

            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(count);
            this.Disconnect();

            return count;
        }
        //HIER EINDIGEN METHODDS VOOR VRAGEN
        

        //HIER BEGINNEN METHODS VOOR ANTWOORDEN
        public DataTable getAllAnswers()
        {
            DataTable answers = new DataTable();
            answers = this.GetDataFromDB("Select a.answer_id, a.question_id, a.answer_text, u.name, u.surname, a.post_date from answer a, users u where u.user_number = a.user_number order by a.post_date DESC;");
            return answers;
        }

        public DataTable getAllAnswersFromSpecificUser(string user_number)
        {
            string sqlString = "select * from answer where user_number = @user_number;";

            DataTable user_answers = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@user_number", user_number);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(user_answers);
            this.Disconnect();

            return user_answers;
        }
        
        public DataTable getAllAnswersForSelectedQuestion(int question_id)
        {
            string sqlString = "Select a.answer_id, a.question_id, a.answer_text, a.post_date, u.name, u.surname, a.user_number from answer a, users u where u.user_number = a.user_number and a.question_id = @question_id order by a.post_date DESC;";

            DataTable relevant_answers = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@question_id", question_id);
            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(relevant_answers);
            this.Disconnect();

            return relevant_answers;
        }

        public int SaveAnswer(Answer answer)
        {
            try
            {
                string sqlString = "INSERT INTO answer (question_id, user_number, answer_text, post_date) VALUES (@question_id, @user_number, @answer_text, @post_date)";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                DateTime date1 = DateTime.Now;

                sqlStatement.Parameters.AddWithValue("@question_id", answer.question_id);
                sqlStatement.Parameters.AddWithValue("@user_number", answer.user_number);
                sqlStatement.Parameters.AddWithValue("@answer_text", answer.answer_text);                
                sqlStatement.Parameters.AddWithValue("@post_date", date1);

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method SaveAnswer: " + ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }

            return 1;
        }

        public int SaveLike(AnswerLike answer_like)
        {
            try
            {
                string sqlString = "INSERT INTO users_like_answer (user_number, answer_id, question_id) VALUES (@user_number, @answer_id, @question_id)";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                sqlStatement.Parameters.AddWithValue("@user_number", answer_like.user_number);
                sqlStatement.Parameters.AddWithValue("@answer_id", answer_like.answer_id);
                sqlStatement.Parameters.AddWithValue("@question_id", answer_like.question_id);

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method SaveAnswerLike: " + ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }

            return 1;
        }

        public DataTable searchAnswerLike(string usernumber, string answer_id)
        {
            string sqlString = "Select a.answer_id, a.question_id, u.user_number from answer a, users u, users_like_answer ua where u.user_number = ua.user_number and a.answer_id = ua.answer_id and a.question_id = ua.question_id and ua.user_number = @actieve_user and ua.answer_id = @answer_id order by a.post_date DESC";

            DataTable likes = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@actieve_user", usernumber);
            sqlStatement.Parameters.AddWithValue("@answer_id", answer_id);

            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(likes);
            this.Disconnect();

            return likes;
        }

        public DataTable getAmountOfLikeofEachAnswer(string answer_id)
        {
            string sqlString = "Select COUNT(u.user_number) as like_count from answer a, users u, users_like_answer ua where u.user_number = ua.user_number and a.answer_id = ua.answer_id and ua.answer_id = @answer_id GROUP BY a.answer_id";

            DataTable count = new DataTable();
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);
            sqlStatement.Parameters.AddWithValue("@answer_id", answer_id);

            SqlDataAdapter data = new SqlDataAdapter(sqlStatement);

            this.Connect();
            data.Fill(count);
            this.Disconnect();

            return count;
        }

        public void CreatePasswordResetCode(string code,string user)
        {
            string sqlString = "INSERT INTO pasword_reset (user_number, code) VALUES (@user_number, @code)";
            SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

            sqlStatement.Parameters.AddWithValue("@user_number", user);
            sqlStatement.Parameters.AddWithValue("@code", code); 
            this.connection.Open();
            sqlStatement.ExecuteNonQuery();           
        }
        public int InsertTeacherOffice(User user)
        {
            try
            {
                string sqlString = "UPDATE users SET office = @office WHERE user_number = @user_number;";
                SqlCommand sqlStatement = new SqlCommand(sqlString, this.connection);

                sqlStatement.Parameters.AddWithValue("@office", user.office);
                sqlStatement.Parameters.AddWithValue("@user_number", user.user_number);

                this.connection.Open();
                sqlStatement.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an exception in method InsertTeacher: " + ex.ToString());
                return -1;
            }
            finally
            {
                this.connection.Close();
            }

            return 1;
        }
    }
}
