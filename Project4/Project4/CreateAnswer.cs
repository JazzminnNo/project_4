﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;

namespace Project4
{
    class CreateAnswer : DialogFragment
    {
        private DatabaseConnection db = new DatabaseConnection();

        private TextView createAnswertitle;
        private TextView beschrijving;
        private TextView errorText;
        private EditText inputAnswer;
        private Button createAnswerButton;
        private Button cancelCreateButton;
        private string userNumber;
        private string questionId;
        private SendMail mail = new SendMail();
        
        public CreateAnswer(string usernumber, int question_id)
        {
            this.userNumber = usernumber;
            //Console.WriteLine("DIT IS USERNUMBEEER IN QA ACTIVITY:"  + this.userNumber);
            this.questionId = question_id.ToString();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.dialog_create_answer, container, false);

            createAnswertitle = view.FindViewById<TextView>(Resource.Id.titleCreateAnswer);
            beschrijving = view.FindViewById<TextView>(Resource.Id.textAnswerBeschrijving);
            errorText = view.FindViewById<TextView>(Resource.Id.createAnswerError);
            inputAnswer = view.FindViewById<EditText>(Resource.Id.inputAnswerTitle);
            createAnswerButton = view.FindViewById<Button>(Resource.Id.createAnswerButton);
            cancelCreateButton = view.FindViewById<Button>(Resource.Id.cancelAnswerButton);
            
            createAnswerButton.Click += createAnswers_Click;
            cancelCreateButton.Click += cancelCreateAnswer_Click;

            return view;
        }

        private void createAnswers_Click(object sender, EventArgs e)
        {
            errorText.TextSize = 12;

            if (inputAnswer.Text == "")
            {
                errorText.Text = "Je moet wel een antwoord invullen.";
            }
            else
            {
                errorText.Text = "";

                Answer answer = new Answer();
                answer.answer_text = inputAnswer.Text.ToString();
                answer.question_id = this.questionId;
                answer.user_number = this.userNumber;
                answer.save();
                string email = answer.getQuestionOwnerEmail(this.questionId);
                mail.sendActivity(email);
                this.Dismiss();
                this.Activity.Recreate();
            }
        }

        private void cancelCreateAnswer_Click(object sender, EventArgs e)
        {
            this.Dismiss();
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }
    }

    public class OnAnswerCreated : EventArgs
    {
        private string mUserNumber;

        public string UserNumber
        {
            get { return mUserNumber; }
            set { mUserNumber = value; }
        }

        public OnAnswerCreated() : base()
        {
            
        }
    }
}