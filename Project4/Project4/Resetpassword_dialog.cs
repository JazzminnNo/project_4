﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Data;

namespace Project4
{
    class Resetpassword_dialog : DialogFragment
    {
        User user;
        private DatabaseConnection db = new DatabaseConnection();

        private TextView titleReset;
        private TextView titleEmail;
        private TextView titleRepeat;
        private EditText inputEmail;
        private EditText inputRepeatemail;
        private Button sendEmail;
        private Button cancelEmail;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_reset_password, container, false);
            titleReset = view.FindViewById<TextView>(Resource.Id.titleResetPassword);
            titleEmail = view.FindViewById<TextView>(Resource.Id.textREmailadress);
            //titleRepeat = view.FindViewById<TextView>(Resource.Id.textHerhaal);
            inputEmail = view.FindViewById<EditText>(Resource.Id.inputEmailadres);
            //inputRepeatemail = view.FindViewById<EditText>(Resource.Id.inputREmailadres);
            sendEmail = view.FindViewById<Button>(Resource.Id.sendEmail);
            sendEmail.Click += sendEmail_Click;
            cancelEmail = view.FindViewById<Button>(Resource.Id.cancelSendemail);
            cancelEmail.Click += cancelEmail_Click;

            return view;
        }

        public void sendEmail_Click(object sender, EventArgs e)
        {
            SendMail mail = new SendMail();
            string email = inputEmail.Text;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            dt = db.GetDataFromDB("select user_number, email from users where email = '"+email+"'");
            if (dt.Rows.Count != 0)
            {
                dt1 = db.GetDataFromDB("select user_number from pasword_reset where user_number = '" + dt.Rows[0][0].ToString() + "'");

                if (dt1.Rows.Count == 0)
                {
                    DateTime date1 = DateTime.Now;
                    string code = email + date1.ToString();
                    string reset_code = db.HashPassword(code);
                    db.CreatePasswordResetCode(reset_code, dt.Rows[0][0].ToString());
                    mail.sendPassReset(reset_code,email);
                    this.Dismiss();
                }
                else
                {
                    Console.WriteLine("...Ja foi enviada o email...");
                }
               
            }
        }

        public void cancelEmail_Click(object sender, EventArgs e)
        {
            this.Dismiss();
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }
    }
}