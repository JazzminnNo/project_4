﻿using System.Data;

namespace Project4
{
    class User
    {
        public string user_number;
        public string name;
        public string surname;
        public string email;
        public string type;
        public string office;
        public string password;
        public int status;
        public string activation_code;
        private DatabaseConnection con;

        public int Save()
        {
            this.con = new DatabaseConnection();
            int status = this.con.UserRegistration(this);

            return status;
        }
        public DataTable getUser(string usernumber)
        {
            this.con = new DatabaseConnection();
            DataTable dt = this.con.getUserbyId(usernumber);
            return dt;
        }
        public int SaveOffice()
        {
            this.con = new DatabaseConnection();
            int status = this.con.InsertTeacherOffice(this);

            return status;
        }


    }
}