using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using DrawerLayout_V7_Tutorial;
using Project4.Adapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using TabLayout = Android.Support.Design.Widget.TabLayout;

namespace Project4
{
    public class OnHomePage : EventArgs
    {
        private string nUserNumber;

        public string Usernumber
        {
            get { return nUserNumber; }
            set { nUserNumber = value; }
        }

        public OnHomePage(string usernumber) : base()
        {
            Usernumber = usernumber;
        }
    }

    [Activity(Label = "Homepage", Icon = "@drawable/hr", Theme = "@style/MyTheme")]

    public class Homepage : AppCompatActivity
    {
        private ImageButton btnHomeCreateQuestion;
        private SupportToolbar mToolbar;
        private MyActionBarDrawerToggle mDrawerToggle;
        private DrawerLayout mDrawerLayout;
        private ListView mLeftDrawer;
        private SwipeRefreshLayout mSwipeRefreshLayout;
        private LinearLayout mPopuFilter;
        private LinearLayout mNieuwFilter;
        private LinearLayout mLayoutFilters;
        private TextView mPopuFilterText;
        private TextView mNieuwFilterText;
        public event EventHandler<OnHomePage> mOnHomepage;
        bool onNieuwFilter = true;
        readonly string idleFilterColour = "#aaa9a9";
        readonly string activeFilterColour = "#ffa228";

        string userNumber;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.homepage);

            //mSwipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeLayout);
            //mSwipeRefreshLayout.SetColorSchemeResources(Android.Resource.Color.HoloBlueBright, Android.Resource.Color.HoloBlueDark, Android.Resource.Color.HoloGreenLight, Android.Resource.Color.HoloRedLight);
            //mSwipeRefreshLayout.Refresh += MSwipeRefreshLayout_Refresh;

            mToolbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            mLeftDrawer = FindViewById<ListView>(Resource.Id.left_drawer);
            btnHomeCreateQuestion = FindViewById<ImageButton>(Resource.Id.fab);
            mPopuFilter = FindViewById<LinearLayout>(Resource.Id.btnFilterPopulairste);
            mNieuwFilter = FindViewById<LinearLayout>(Resource.Id.btnFilterNieuwste);
            mPopuFilterText = FindViewById<TextView>(Resource.Id.homeSorteerPopulairQuestion);
            mNieuwFilterText = FindViewById<TextView>(Resource.Id.homeSorteerNieuwQuestion);
            mLayoutFilters = FindViewById<LinearLayout>(Resource.Id.linearLayoutFilters);

            SetSupportActionBar(mToolbar);

            // Get the ViewPager and set it's PagerAdapter so that it can display items
            ViewPager viewPager = (ViewPager)FindViewById(Resource.Id.viewpager);
            viewPager.Adapter = new MyAdapter(SupportFragmentManager, this);
            TabLayout tabLayout = (TabLayout)FindViewById(Resource.Id.sliding_tabs);
            tabLayout.SetupWithViewPager(viewPager);

            tabLayout.SetSelectedTabIndicatorColor(Color.ParseColor("#FFFFFF"));
            
            List<string> nav_items = new List<string>() {"Profiel", "Uitloggen" };


            string usernumber = Intent.Extras.GetString("usernumber");
            Console.WriteLine("HAAAAAAAAAAALO VENUIT HOMEPAGEE DIT IS USERNUMBER NAAR PROFIEL: " + usernumber);
        
            mLeftDrawer.Adapter = new NaviAdapter(this, nav_items, usernumber);

            mDrawerToggle = new MyActionBarDrawerToggle(
                    this,
                    mDrawerLayout,
                    Resource.String.hometitle,
                    Resource.String.hometitle
                    );

            mDrawerLayout.AddDrawerListener(mDrawerToggle);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetTitle(Resource.String.hometitle);
            mDrawerToggle.SyncState();

            btnHomeCreateQuestion.Click += btnHomeCreateQuestion_Click;
            mPopuFilter.Click += mPopuFilter_Click;
            mNieuwFilter.Click += mNieuwFilter_Click;

            onNieuwFilter = Intent.Extras.GetBoolean("boolFilter");
            if (onNieuwFilter)
            {
                mNieuwFilter.SetBackgroundColor(Color.ParseColor(activeFilterColour));
                mPopuFilter.SetBackgroundColor(Color.ParseColor(idleFilterColour));
                mNieuwFilterText.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);     
            }
            else
            {
                mPopuFilter.SetBackgroundColor(Color.ParseColor(activeFilterColour));
                mNieuwFilter.SetBackgroundColor(Color.ParseColor(idleFilterColour));
                mPopuFilterText.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
            };
            viewPager.PageSelected += viewPager_PageSelected;
        }
       
        private void viewPager_PageSelected(object sender, ViewPager.PageSelectedEventArgs e)
        {
            if(e.Position != 0)
            {
                mPopuFilter.Visibility = ViewStates.Invisible;
                mNieuwFilter.Visibility = ViewStates.Invisible;
                mPopuFilterText.Visibility = ViewStates.Invisible;
                mNieuwFilterText.Visibility = ViewStates.Invisible;
            }
            else
            {
                mPopuFilter.Visibility = ViewStates.Visible;
                mNieuwFilter.Visibility = ViewStates.Visible;
                mPopuFilterText.Visibility = ViewStates.Visible;
                mNieuwFilterText.Visibility = ViewStates.Visible;

            }        

        }

        private void mNieuwFilter_Click(object sender, EventArgs e)
        {
            onNieuwFilter = true;
            
            userNumber = Intent.Extras.GetString("usernumber");
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", userNumber);
            intent.PutExtra("boolFilter", onNieuwFilter);
            StartActivity(intent);
            Finish();
        }

        private void mPopuFilter_Click(object sender, EventArgs e)
        {
            onNieuwFilter = false;

            userNumber = Intent.Extras.GetString("usernumber");
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", userNumber);
            intent.PutExtra("boolFilter", onNieuwFilter);
            StartActivity(intent);
            Finish();
        }

        private void btnHomeCreateQuestion_Click(object sender, EventArgs e)
        {
            userNumber = Intent.Extras.GetString("usernumber");
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            dialog_CreateQuestion createQuestionDialog = new dialog_CreateQuestion(userNumber);
            createQuestionDialog.Show(transaction, "dialog fragment");
        }

        private void MSwipeRefreshLayout_Refresh(object sender, System.EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RunOnUiThread(() => { mSwipeRefreshLayout.Refreshing = false; });
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            mDrawerToggle.OnOptionsItemSelected(item);
            return base.OnOptionsItemSelected(item);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            mDrawerToggle.SyncState();
        }
    }
}