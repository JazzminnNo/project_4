﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;

namespace Project4
{
    class dialog_CreateQuestion : DialogFragment
    {
        private DatabaseConnection db = new DatabaseConnection();

        private TextView createQuestion;
        private TextView categorie;
        private TextView titleQuestion;
        private TextView beschrijving;
        private TextView errorText;

        private EditText inputTitle;
        private EditText inputBeschrijving;

        private Spinner categorieSpinner;
        private bool selectSpinner = false;

        private Button createQuestionButton;
        private Button cancelCreate;
        private string selectedCategory;

        private string userNumberFromUser;

        public dialog_CreateQuestion(string usernumber)
        {
            userNumberFromUser = usernumber;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_create_question, container, false);
            createQuestion = view.FindViewById<TextView>(Resource.Id.titleCreateQuestion);
            categorie = view.FindViewById<TextView>(Resource.Id.textCategorie);
            titleQuestion = view.FindViewById<TextView>(Resource.Id.textTitleQuestion);
            beschrijving = view.FindViewById<TextView>(Resource.Id.textBeschrijving);
            errorText = view.FindViewById<TextView>(Resource.Id.createQuestionError);

            inputTitle = view.FindViewById<EditText>(Resource.Id.inputTitle);
            inputBeschrijving = view.FindViewById<EditText>(Resource.Id.inputBeschrijving);

            categorieSpinner = view.FindViewById<Spinner>(Resource.Id.categorieDrop);
            string firstItem = categorieSpinner.SelectedItem.ToString();
            
            categorieSpinner.ItemSelected += (s, e) =>
            {
                if (firstItem.Equals(categorieSpinner.SelectedItem.ToString()))
                {
                    // Toast.MakeText(this.Activity, "Please select a categorie", ToastLength.Short).Show(); 
                }
                else
                {
                    selectSpinner = true;
                    selectedCategory = categorieSpinner.SelectedItem.ToString();
                }
            };

            createQuestionButton = view.FindViewById<Button>(Resource.Id.createQuestionButton);
            createQuestionButton.Click += createQuestions_Click;

            cancelCreate = view.FindViewById<Button>(Resource.Id.cancelQuestionButton);
            cancelCreate.Click += cancelCreateAnswer_Click;

            return view;
        }

        private void createQuestions_Click(object sender, EventArgs e)
        {
            //Console.WriteLine("Category : "+ categorie.Text);
            //Console.WriteLine("titleQuestion : " + inputTitle.Text);
            //Console.WriteLine("beschrijving : " + inputBeschrijving.Text);
            errorText.TextSize = 12;
            
            if (inputTitle.Text == "")
             {
                errorText.Text = "Schrijf een titel voor je vraag.";
             }
            else if(inputBeschrijving.Text == "")
            {
                errorText.Text = "Vul de beschrijving van de vraag in.";
            }
            else if (!selectSpinner)
            {
                errorText.Text = "Kies een categorie.";
            }
            else if (inputTitle.Text.Length > 50)
            {
                errorText.Text = "De titel is te lang. Maximaal 50 karakters zijn toegestaan.";
            }
            else if (inputBeschrijving.Text.Length > 600)
            {
                errorText.Text = "De beschrijving is te lang. Maximaal 600 karakters zijn toegestaan";
                errorText.TextSize = 10;
            }
            else
            {
                errorText.Text = "";

                Question question = new Question();
                question.title = inputTitle.Text;   
                question.question_text = inputBeschrijving.Text;
                question.category = selectedCategory;
                question.user_number = userNumberFromUser;
                question.save();

                this.Activity.Recreate();
                this.Dismiss();
            }
        }

        private void cancelCreateAnswer_Click(object sender, EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            this.Dismiss();
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }
    }
}