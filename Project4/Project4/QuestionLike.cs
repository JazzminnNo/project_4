﻿using System.Data;

namespace Project4
{
    class QuestionLike
    {
        public string user_number;
        public int question_id;
        DatabaseConnection con;

        public QuestionLike()
        {
            this.con = new DatabaseConnection();
        }
        
        public int SaveLike()
        {
            return this.con.SaveLike(this);
        }

        public DataTable SearchQuestionLike(string usernumber, string questionid)
        {
            return this.con.searchQuestionLike(usernumber, questionid);
        }

        public DataTable AmountOfLikeofEachQuestion(int questionid)
        {
            return this.con.getAmountOfLikeofEachQuestion(questionid);
        }
    }
}