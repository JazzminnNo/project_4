﻿using System;
using System.Data;

namespace Project4
{
    public class Question
    {
        public int category_id;
        public string user_number;        
        public string question_text;
        public string post_date; 

        public string id;
        public string category;
        public string user;
        public string title;
        public string text;
        public string date;
        DatabaseConnection con;

        
        public Question() {
            this.con = new DatabaseConnection();
        }

        public DataTable getAll(bool filter = true)
        {
            return con.getAllQuestion(filter); 
        }

        public DataTable getAllQuestionsFromCategory(int category_id, bool filter = true)
        {
            return con.getAllQuestionsFromCategory(category_id, filter);
        }

        public DataTable getSelectedQuestion(int question_id)
        {
            return con.getSelectedQuestion(question_id);
        }

        public int save()
        {
            return con.SaveQuestion(this);
        }

        public string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "zojuist";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minuut geleden";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minuten geleden",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 uur geleden";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} uren geleden",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "gisteren";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} dagen geleden",
                    dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weken geleden",
                    Math.Ceiling((double)dayDiff / 7));
            }
            return null;
        }
    }
}