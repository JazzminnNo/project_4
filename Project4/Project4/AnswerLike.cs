﻿using System.Data;

namespace Project4
{
    class AnswerLike
    {
        public string user_number;
        public string answer_id;
        public string question_id;
        DatabaseConnection con;

        public AnswerLike()
        {
            this.con = new DatabaseConnection();
        }

        public int save()
        {
            return this.con.SaveLike(this);
        }
       
        public DataTable searchLike(string usernumber, string answerid)
        {
            return con.searchAnswerLike(usernumber, answerid);
        }

        public DataTable getLike(string answer_id)
        {
            return con.getAmountOfLikeofEachAnswer(answer_id);
        }
    }
}