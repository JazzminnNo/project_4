﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Project4
{
    public class OnSignUpEventArgs : EventArgs
    {
        private string mName, mSurname, mID, mEmail, mPass;

        public string SignUpName
        {
            get { return mName; }
            set { mName = value; }
        }

        public string SignUpSurname
        {
            get { return mSurname; }
            set { mSurname = value; }
        }

        public string SignUpID
        {
            get { return mID; }
            set { mID = value; }
        }

        public string SignUpEmail
        {
            get { return mEmail; }
            set { mEmail = value; }
        }

        public string SignUpPassword
        {
            get { return mPass; }
            set { mPass = value; }
        }

        public OnSignUpEventArgs(string name, string surname, string id, string email, string password) : base()
        {
            SignUpName = name;
            SignUpSurname = surname;
            SignUpID = id;
            SignUpEmail = email;
            SignUpPassword = password;
        }
    }

    class dialog_SignUp : DialogFragment
    {
        private EditText textName;
        private EditText tussenvoegsel;
        private EditText textSurname;
        private EditText textID;
        private EditText textEmailAdress;
        private EditText textPassword;
        private EditText textRepeatPassword;
        private TextView textErrorRegister;
        private Button btnSignUp;
        private DatabaseConnection db = new DatabaseConnection();
        public event EventHandler<OnSignUpEventArgs> mOnRegistrationComplete;
        public User user;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.dialog_sign_up, container, false);

            textName = view.FindViewById<EditText>(Resource.Id.sign_up_txtName);
            tussenvoegsel = view.FindViewById<EditText>(Resource.Id.sign_up_tussenvoegsel);
            textSurname = view.FindViewById<EditText>(Resource.Id.sign_up_txtSurname);
            textID = view.FindViewById<EditText>(Resource.Id.sign_up_txtID);
            textEmailAdress = view.FindViewById<EditText>(Resource.Id.sign_up_txtEmail);
            textPassword = view.FindViewById<EditText>(Resource.Id.sign_up_textPassword);
            textRepeatPassword = view.FindViewById<EditText>(Resource.Id.sign_up_textRepeatPassword);

            textErrorRegister = view.FindViewById<TextView>(Resource.Id.txtErrorRegister);

            btnSignUp = view.FindViewById<Button>(Resource.Id.sign_up_signUpBtn);

            btnSignUp.Click += btnSignUpRegisterPage_Click;

            return view;
        }

        private void btnSignUpRegisterPage_Click(object sender, EventArgs e)

        {
            textErrorRegister.TextSize = 14;            

            bool isStudentEmail = false;
            bool isTeacherEmail = false;
            bool isTeacherCode = false;

            if (IsAllDigits(textID.Text))
            {
                isStudentEmail = true;
                isTeacherEmail = false;
            }              
            else if (IsAllLetters(textID.Text))
            {
                isStudentEmail = false;
                isTeacherEmail = true;
            }
            else
            {
                isStudentEmail = false;
                isTeacherEmail = false;
            }

            if(textName.Text == "" || textSurname.Text == "" || textID.Text == "" || textEmailAdress.Text == "" || textPassword.Text == "" || textRepeatPassword.Text == "")
            {
                textErrorRegister.Text = "Er is een leeg veld. Vul alle velden in.";
            }
            else if (!Regex.IsMatch(textName.Text, @"^[\p{L}]+$"))
            {
                textErrorRegister.Text = "Voornaam bevat geen/ongeldige tekens.";
            }
            else if(!Regex.IsMatch(tussenvoegsel.Text, @"^[a-zA-Z\s]*$")){
                textErrorRegister.Text = "Tussenvoegsel bevat geen/ongeldige tekens.";
            }
            else if (!Regex.IsMatch(textSurname.Text, @"^[\p{L}]+$"))
            {
                textErrorRegister.Text = "Achternaam bevat geen/ongeldige tekens.";
            }
            else if(textID.Text.Length != 7 && isStudentEmail)
            {
                if (textID.Text.Length < 7)
                    textErrorRegister.Text = "Het studentnummer is te kort.";
                else
                    textErrorRegister.Text = "Het studentnummer is te lang.";
            }
            else if(textID.Text.Length != 5 && isTeacherEmail)
            {
                if (textID.Text.Length < 5)
                    textErrorRegister.Text = "De docentcode is te kort.";
                else
                    textErrorRegister.Text = "De docentcode is te lang.";
            }
            else if(!isStudentEmail && !isTeacherEmail)
            {
                textErrorRegister.Text = "De student/docentcode klopt niet.";
            }
            else if (scan_email(textEmailAdress.Text).Item2 != "@hr.nl")
            {
                textErrorRegister.Text = "Het emailadres moet eindigen op @hr.nl";
            }
            else if(textEmailAdress.Text.Length != 13 && isStudentEmail)
            {
                if (textEmailAdress.Text.Length < 13)
                    textErrorRegister.Text = "Het opgegeven emailadres is te kort.";
                else
                    textErrorRegister.Text = "Het opgegeven emailadres is te lang";
            }
            else if (IsAllDigits(scan_email(textEmailAdress.Text).Item1) && isTeacherEmail)
            {
                textErrorRegister.Text = "Vul een docentemailadres in.";
            }
            else if (textID.Text != scan_email(textEmailAdress.Text).Item1 && isStudentEmail)
            {
                textErrorRegister.Text = "Het studentnummer komt niet overeen met het emailadres.";
                textErrorRegister.TextSize = 10;
            }
            else if (!(textPassword.Text.Length >= 6 && textPassword.Text.Length <= 30))
            {
                textErrorRegister.Text = "Wachtwoord moet minimaal 6 en maximaal 30 tekens lang zijn.";
                textErrorRegister.TextSize = 10;
            }
            else if(textPassword.Text != textRepeatPassword.Text)
            {
                textErrorRegister.Text = "De wachtwoorden komen niet overeen.";
            }
            else
            {
                textName.Text = correct_name(textName.Text);
                textSurname.Text = correct_name(textSurname.Text);

                user = new User();
                user.user_number = textID.Text;
                user.name = textName.Text;
                user.surname = tussenvoegsel.Text + " " + textSurname.Text;
                user.email = textEmailAdress.Text;
                user.password = textPassword.Text;
                int status = user.Save();

                if (status == 1)
                {
                    textErrorRegister.Text = "Er bestaat al een account met deze code.";
                    textErrorRegister.TextSize = 12;
                }
                else if (status == -1)
                {
                    textErrorRegister.Text = "Er is een probleem met het registreren, probeer het a.u.b. later...";
                    textErrorRegister.TextSize = 12;
                }
                else
                {
                    this.Dismiss();
                    //tussenvoegsel.Text = tussenvoegsel.Text + " ";
                    //textSurname.Text = tussenvoegsel.Text + " " + textSurname.Text;
                    mOnRegistrationComplete.Invoke(this, new OnSignUpEventArgs(textName.Text, textSurname.Text, textID.Text, textEmailAdress.Text, textPassword.Text));
                }
            }
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation_sign_up;
        }

        public string correct_name(string name)
        {
            switch (name)
            {
                case null: throw new ArgumentNullException(nameof(name));
                case "": throw new ArgumentException($"{nameof(name)} cannot be empty", nameof(name));
                default: return name.First().ToString().ToUpper() + name.Substring(1).ToLower();
            }
        }

        public static bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public static bool IsAllLetters(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsLetter(c))
                    return false;
            }
            return true;
        }
        
        public (string, string) scan_email(string email)
        {
            string emailPrefix = "";
            string emailSuffix = "";

            Char delimiter = '@';
            foreach(char s in email){
                if(s == '@')
                {
                    String[] substrings = email.Split(delimiter);
                    emailPrefix = substrings[0];
                    emailSuffix = "@" + substrings[1];
                    break;
                }
            }                  
            return (emailPrefix, emailSuffix);
        }
    }
}

