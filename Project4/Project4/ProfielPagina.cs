﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

namespace Project4
{
    [Activity(Label = "Profiel", Icon = "@drawable/hr", Theme = "@style/MyTheme")]
    class ProfielPagina : AppCompatActivity
    {
        private TextView mNaam;
        private TextView mNumber;
        private TextView mEmail;
        private TextView mType;
        private TextView mOffice;
        private TextView officeInputTxt;
        private EditText officeInputEditTxt;
        private Button officeSave;
        private TextView mOfficeHelperTxt;
        private SupportToolbar mToolbar;
        string usernumber;
        User mUser;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.profiel);

            FindViews();
            //mToolbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            SetSupportActionBar(mToolbar);

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);        
            

            usernumber = Intent.Extras.GetString("userNumber");
            Console.WriteLine("HAALLO WAT ZIT HIER IN VANUIT PROFIELPAGIN: " + usernumber);
            DataTable dt = new DataTable();
            User u = new User();
            dt = u.getUser(usernumber);
            foreach(DataRow row in dt.Rows)
            {
                Console.WriteLine("DIT IS VANUIT PROFIELPAGINAAAAAAAAAAAAAAA LAAT ME EEN RIJ KOLOM ZIEN" + row[0].ToString() + " "  +row[5].ToString()); 
            }

            foreach(DataRow row in dt.Rows)
            {
                User nUser = new User();
                nUser.user_number = row[0].ToString();
                nUser.name = row[1].ToString();
                nUser.surname = row[2].ToString();
                nUser.email = row[3].ToString();
                nUser.type = row[4].ToString();
                nUser.office = row[5].ToString();
                mUser = nUser;
            }
            BindData();
        }
        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{
        //    switch (item.ItemId)
        //    {
        //        case (Resource.Id.homeAsUp)
        //            var intent = new Intent(this, typeof(Homepage));
        //            StartActivity(intent);
        //            Finish();
        //            return true;
        //        default:
        //            return base.OnOptionsItemSelected(item);
        //    }
        //}
        public override bool OnSupportNavigateUp()
        {
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", usernumber);
            StartActivity(intent);
            Finish();
            return true;
        }
        public override void OnBackPressed()
        {
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", usernumber);
            StartActivity(intent);
            Finish();
        }
        private void BindData()
        {
            if (mUser.type == "Leraar")
            {
                mOffice.Visibility = ViewStates.Visible;
                mOfficeHelperTxt.Visibility = ViewStates.Visible;
                officeInputTxt.Visibility = ViewStates.Visible;
                officeInputEditTxt.Visibility = ViewStates.Visible;
                officeSave.Visibility = ViewStates.Visible;
                officeSave.Click += officeSave_Click;

            }
            mNumber.Text = mUser.user_number;
            mNaam.Text = mUser.name + " " + mUser.surname;
            mEmail.Text = mUser.email;
            mType.Text = mUser.type;
            mOffice.Text = mUser.office;  
            if(mUser.office == "")
            {
                mOffice.Text = "(Geen office)";
            }
        }

        private void officeSave_Click(object sender, EventArgs e)
        {
            if((officeInputEditTxt.Text != "") && (officeInputEditTxt.Text.Length <= 50))
            {
                User u = new User();
                u.user_number = usernumber;
                u.office = officeInputEditTxt.Text;
                u.SaveOffice();
                officeInputEditTxt.Text = "";
                this.Recreate();
            }
        }

        private void FindViews()
        {
            mNaam = FindViewById<TextView>(Resource.Id.profielNaam1);
            mNumber = FindViewById<TextView>(Resource.Id.profielNummer1);
            mEmail = FindViewById<TextView>(Resource.Id.profielEmail1);
            mType = FindViewById<TextView>(Resource.Id.profielType1);
            mOffice = FindViewById<TextView>(Resource.Id.profielOffice1);
            officeInputTxt = FindViewById<TextView>(Resource.Id.officeTxt1);
            officeInputEditTxt = FindViewById<EditText>(Resource.Id.officeInput1);
            officeSave = FindViewById<Button>(Resource.Id.officeSaveBtn1);
            mToolbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            mOfficeHelperTxt = FindViewById<TextView>(Resource.Id.officeHelperTxt);
        }
    }
}