﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Data;

namespace Project4
{
    public class OnSignInEventArgs : EventArgs
    {
        private string mUserNumber;

        public string UserNumber
        {
            get { return mUserNumber; }
            set { mUserNumber = value; }
        }

        public OnSignInEventArgs(string usernumber) : base()
        {
            UserNumber = usernumber;
        }
    }

    class dialog_SignIn : DialogFragment
    {
        private DatabaseConnection db = new DatabaseConnection();
        private TextView txtErrorLoginPage;
        private EditText txtEmailLoginPage;
        private EditText txtPassLoginPage;
        private Button btnForgetPassLoginPage;
        private Button btnSignInLoginPage;
        public event EventHandler<OnSignInEventArgs> mOnLoginComplete;      

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.dialog_sign_in, container, false);

            txtErrorLoginPage = view.FindViewById<TextView>(Resource.Id.txtErrorLogin);
            txtEmailLoginPage = view.FindViewById<EditText>(Resource.Id.txtEmail);
            txtPassLoginPage = view.FindViewById<EditText>(Resource.Id.txtPassword);
            btnForgetPassLoginPage = view.FindViewById<Button>(Resource.Id.btnFPassword);
            btnSignInLoginPage = view.FindViewById<Button>(Resource.Id.btnSignIn);

            btnForgetPassLoginPage.Click += btnForgetPassLoginPage_Click;
            btnSignInLoginPage.Click += btnSignInLoginPage_Click;
            
            return view;
        }

        private void btnSignInLoginPage_Click(object sender, EventArgs e)
        {
            DataTable data = db.UserLogin(txtEmailLoginPage.Text, txtPassLoginPage.Text);
           
            if (data.Rows.Count > 0)
            {
                User user = new User();

                foreach (DataRow row in data.Rows)
                {
                    user.user_number = row[0].ToString();
                    user.name = row[1].ToString();
                    user.surname = row[2].ToString();
                    user.email = row[3].ToString();
                    user.type = row[4].ToString();
                    user.office = row[5].ToString();
                    user.status = (int)row[6];
                    user.activation_code = row[7].ToString();
                }

                if (user.status == 1) {
                    mOnLoginComplete.Invoke(this, new OnSignInEventArgs(user.user_number));
                    this.Dismiss();
                    Console.WriteLine("LOGIN GELUKT");
                }
                else if(user.status == 0)
                {
                    txtErrorLoginPage.Text = "Je account is nog niet geactiveerd, check je email.";
                }

            }
           else
           {
                if (txtEmailLoginPage.Text.Length == 0 && txtPassLoginPage.Text.Length == 0)
                {
                    txtErrorLoginPage.Text = "Velden zijn niet ingevuld.";
                }
                else if (txtEmailLoginPage.Text.Length == 0)
                {
                    txtErrorLoginPage.Text = "Er is geen schoolaccount ingevuld.";
                }
                else if (txtPassLoginPage.Text.Length == 0)
                {
                    txtErrorLoginPage.Text = "Er is geen wachtwoord ingevuld.";
                }
                else
                {
                    txtErrorLoginPage.Text = "Combinatie schoolaccount/wachtwoord is onjuist.";
                }
           }     
        }

        private void btnForgetPassLoginPage_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Hallo mama");
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            Resetpassword_dialog reset_dialog = new Resetpassword_dialog();
            reset_dialog.Show(transaction, "dialog fragment");
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Hides the above bar for this dialog
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation_sign_in; //Sets the animation for this dialog
        }
    }
}