﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Project4.Adapters;
using System;
using System.Collections.Generic;
using System.Data;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

namespace Project4
{
    [Activity(Theme = "@style/MyTheme")]

    public class QuestionAnswerOverviewActivity : AppCompatActivity
    {
        private ListView qaQuestionListView;
        private ListView qaAnswerListView;
        private TextView qaAmountAnswersQuestion;
        private TextView qaPostDateQuestion;
        private ImageButton qaBtnCreateAnswer;
        private LinearLayout mLinearLayout;
        private ImageButton qaBtnLikeQuestion;
        private TextView qaAmountOfLikesQuestion;
        private TextView mToastTxt;
        private SupportToolbar mToolbar;

        List<Question> selected_question = new List<Question>();
        DataTable dt = new DataTable();
        Question q = new Question();

        List<Answer> relevant_answers = new List<Answer>();
        DataTable dt2 = new DataTable();
        Answer a = new Answer();
        QuestionLike ql = new QuestionLike();

        DataTable dt3 = new DataTable();

        DataTable dt4 = new DataTable();

        AnswerFocusAdapter myansweradap;

        string active_usernumber;
        int selected_id;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            relevant_answers.Clear();

            SetContentView(Resource.Layout.qa_frame_layout);

            selected_question.Clear();
            FindViews();

            SetSupportActionBar(mToolbar);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);        

            qaBtnCreateAnswer.Click += qaBtnCreateAnswer_click;
            
            selected_id = Intent.Extras.GetInt("selectedQuestionId");
            active_usernumber = Intent.Extras.GetString("activeUsernumber");
            //Console.WriteLine("IS HET  GEEEELUKKTKTKTKTKTJKEDNKFSNNJ: "+ active_usernumber);

            dt = q.getSelectedQuestion(selected_id);
            foreach (DataRow row in dt.Rows)
            {
                Question added_question = new Question();
                added_question.id = row[0].ToString();
                added_question.title = row[1].ToString();
                added_question.text = row[2].ToString();
                added_question.category = row[3].ToString();
                added_question.user = row[4].ToString() + " " + row[5].ToString();
                added_question.date = row[6].ToString();

                selected_question.Add(added_question);
            }

            //relevant_answers.Clear();
            dt2 = a.getAllAnswersForSelectedQuestion(selected_id);
            

            foreach (DataRow row in dt2.Rows)
            {
                Answer relevant_answer = new Answer();
                relevant_answer.answer_id = row[0].ToString();
                relevant_answer.question_id = row[1].ToString();
                relevant_answer.answer_text = row[2].ToString();
                relevant_answer.post_date = q.GetPrettyDate(DateTime.Parse(row[3].ToString()));
                relevant_answer.user_name = row[4].ToString() + " " + row[5].ToString();
                relevant_answer.user_number = row[6].ToString();

                Console.WriteLine("Ik ben een answer: "+relevant_answer.answer_text);

                relevant_answers.Add(relevant_answer);
            }

           /* foreach (Answer answer in relevant_answers)
            {
                Console.WriteLine("FROM QA OVERVIEW ACTIVITY: ");
                Console.WriteLine("answer_id: " + answer.answer_id + "question_id: " + answer.question_id + "answer_text: " + answer.answer_text + "answer_date: " + answer.post_date + "username: " + answer.user_name + "usernumber: " + answer.user_number);
            }*/

            BindData();

            //qaAnswerListView.SetScrollContainer(false);
            mLinearLayout.SetScrollContainer(true);

            qaAnswerListView.Adapter = new AnswerFocusAdapter(this, relevant_answers, active_usernumber, selected_id);

            qaQuestionListView.Adapter = new QuestionFocusAdapter(this, selected_question);
        }

        public override bool OnSupportNavigateUp()
        {
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", active_usernumber);
            StartActivity(intent);
            Finish();
            return true;
        }
        public override void OnBackPressed()
        {
            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", active_usernumber);
            StartActivity(intent);
            Finish();
        }

        private void qaBtnCreateAnswer_click(object sender, EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            CreateAnswer createAnswerDialog = new CreateAnswer(active_usernumber, selected_id);
            createAnswerDialog.Show(transaction, "dialog fragment");
        }

        private void FindViews()
        {
            qaQuestionListView = FindViewById<ListView>(Resource.Id.qaQuestionListView);
            qaAnswerListView = FindViewById<ListView>(Resource.Id.qaAnswerListView);
            qaAmountAnswersQuestion = FindViewById<TextView>(Resource.Id.qaAmountAnswersQuestion);
            qaAmountOfLikesQuestion = FindViewById<TextView>(Resource.Id.qaAmountOfLikesQuestion);
            qaBtnCreateAnswer = FindViewById<ImageButton>(Resource.Id.qaBnCreateAnswer);
            qaAnswerListView = FindViewById<ListView>(Resource.Id.qaAnswerListView);
            mLinearLayout = FindViewById<LinearLayout>(Resource.Id.container);
            qaPostDateQuestion = FindViewById<TextView>(Resource.Id.qaPostDateQuestion);
            qaBtnLikeQuestion = FindViewById<ImageButton>(Resource.Id.btnLike);
            mToastTxt = FindViewById<TextView>(Resource.Id.forToastTxt);
            mToolbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
        }

        private void BindData()
        {
            qaAmountAnswersQuestion.Text = relevant_answers.Count.ToString();
            DataTable dt5 = new DataTable();

            dt5 = q.getSelectedQuestion(selected_id);
            foreach (DataRow row in dt.Rows)
            {
               qaPostDateQuestion.Text = row[6].ToString();
            }

            qaBtnLikeQuestion.Click += QaBtnLikeQuestion_Click;

            dt4 = ql.AmountOfLikeofEachQuestion(selected_id);
            string amountOfLikesSpecificQuestion = "0";
            foreach (DataRow row in dt4.Rows)
            {
                amountOfLikesSpecificQuestion = "";
                amountOfLikesSpecificQuestion = amountOfLikesSpecificQuestion + row[0].ToString();
            }
            Console.WriteLine("FROM QA OVERVIEW ACTIVITY THIS IS THE AMOUNT OF LIKES OF THIS QUESTION:" + amountOfLikesSpecificQuestion);

            qaAmountOfLikesQuestion.Text = amountOfLikesSpecificQuestion;
        }
        
        private void QaBtnLikeQuestion_Click(object sender, EventArgs e)
        {
            dt3 = ql.SearchQuestionLike(active_usernumber, selected_id.ToString());
            if (dt3.Rows.Count > 0)
            {

                mToastTxt.Text = "Je hebt deze vraag AL geliket";
                Toast.MakeText(this, "Je hebt deze vraag AL geliket", ToastLength.Short).Show();
            }
            else
            {
                QuestionLike questionLike = new QuestionLike();
                questionLike.user_number = active_usernumber;
                questionLike.question_id = selected_id;

                questionLike.SaveLike();
                mToastTxt.Text = "Je hebt deze vraag geliket";
                Toast.MakeText(this, "Je hebt deze vraag geliket", ToastLength.Short).Show();
                this.Recreate();
            }
        }
    }
}