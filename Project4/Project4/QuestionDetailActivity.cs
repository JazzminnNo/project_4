﻿using Android.App;
using Android.OS;
using Android.Widget;
using System;

namespace Project4
{
    [Activity(Label = "QuestionDetailActivity")]
    public class QuestionDetailActivity : Activity
    {
        private TextView questionUserNumberTextView;
        private TextView questionTitleTextView;
        private TextView questionDescriptionTextView;
        private Button questionDislikeBtn;
        private Button questionLikeBtn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.question_detail_view);
                        
            FindViews();
            HandleEvents();
        }

        private void FindViews()
        {
            questionUserNumberTextView = FindViewById<TextView>(Resource.Id.questionUserNumberTextView);
            questionTitleTextView = FindViewById<TextView>(Resource.Id.questionTitleTextView);
            questionDescriptionTextView = FindViewById<TextView>(Resource.Id.questionDescriptionTextView);
            questionLikeBtn = FindViewById<Button>(Resource.Id.questionLikeBtn);
            questionDislikeBtn = FindViewById<Button>(Resource.Id.questionDislikeBtn);
        }

        private void HandleEvents()
        {
            questionLikeBtn.Click += QuestionLikeBtn_Click;
            questionDislikeBtn.Click += QuestionDislikeBtn_Click;
        }

        private void QuestionDislikeBtn_Click(object sender, EventArgs e)
        {
            var dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("Question disliked");
            dialog.SetMessage("You did not like this question.");
            dialog.Show();
        }

        private void QuestionLikeBtn_Click(object sender, EventArgs e)
        {
            var dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("Question liked");
            dialog.SetMessage("You liked this question.");
            dialog.Show();
        }
    }
}