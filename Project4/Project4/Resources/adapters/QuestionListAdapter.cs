﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Project4.Resources.adapters
{
    public class QuestionListAdapter: BaseAdapter<Question>
    {
        List<Question> items;
      
        Activity context;

        public QuestionListAdapter(Activity context, List<Question> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Question this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.question_overview_row_view, null);
            }

            convertView.FindViewById<TextView>(Resource.Id.questionTitleOverview).Text = item.title;
            convertView.FindViewById<TextView>(Resource.Id.questionUserIDOverview).Text = "Door: " + item.user_number;
            convertView.FindViewById<TextView>(Resource.Id.questionDescriptionOverview).Text = item.question_text;
            convertView.FindViewById<TextView>(Resource.Id.questionCategoryOverview).Text = item.category_id;
            return convertView;
        }
    }
}