﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;
using System.Threading;

namespace Project4
{
    [Activity(Label = "VraagMe Q&A", MainLauncher = true, Theme = "@style/MyTheme")]

    public class MainActivity : AppCompatActivity
    {
        private Button mBtnSignIn;
        private Button mBtnSignUp;
        private DatabaseConnection db = new DatabaseConnection();
        bool inlogcomplete = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //Als je ingelogd bent.
            if (inlogcomplete)
            {
                SetContentView(Resource.Layout.home_tab);
            }

            SetContentView(Resource.Layout.sign_in_sign_up);

            FindViews();

            mBtnSignIn.Click += mBtnSignIn_Click;
            mBtnSignUp.Click += mBtnSignUp_Click;
        }

        protected void FindViews()
        {
            mBtnSignIn = FindViewById<Button>(Resource.Id.btnSignIn);
            mBtnSignUp = FindViewById<Button>(Resource.Id.btnSignUp);
        }

        void mBtnSignIn_Click(object sender, EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            dialog_SignIn signInDialog = new dialog_SignIn();
            signInDialog.Show(transaction, "dialog fragment");
            signInDialog.mOnLoginComplete += signInDialog_mOnLoginComplete;
        }

        //LOGIN EVENT
        private void signInDialog_mOnLoginComplete(object sender, OnSignInEventArgs e)
        {
            string userNumber = e.UserNumber;
            inlogcomplete = true;
            Toast.MakeText(this, "U bent ingelogd", ToastLength.Short).Show();

            var intent = new Intent(this, typeof(Homepage));
            intent.PutExtra("usernumber", userNumber);
            StartActivity(intent);
            Finish();
        }

        private void mBtnSignUp_Click(object sender, EventArgs e)
        {
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            dialog_SignUp signUpDialog = new dialog_SignUp();
            signUpDialog.Show(transaction, "dialog fragment");
            signUpDialog.mOnRegistrationComplete += signUpDialog_mOnRegistrationComplete;
        }

        //REGISTRATION EVENT
        private void signUpDialog_mOnRegistrationComplete(object sender, OnSignUpEventArgs e)
        {
            Toast.MakeText(this, "Registratie voltooid. Ontvangst van activatiemail kan tot 5 minuten duren", ToastLength.Long).Show();
          
            //Ingevulde informatie die beschikbaar is na drukken registratieknop
            string userFirstName = e.SignUpName;
            string userSurname = e.SignUpSurname;
            string userID = e.SignUpID;
            string userEmail = e.SignUpEmail;
            string userPassword = e.SignUpPassword;
        }
    }
}