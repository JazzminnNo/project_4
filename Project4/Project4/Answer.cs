﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Project4
{
    public class Answer
    {
        public string answer_id;
        public string question_id;
        public string user_number;
        public string user_name;
        public string answer_text;
        public string post_date;
        DatabaseConnection con;

        public Answer()
        {
            this.con = new DatabaseConnection();
        }

        public DataTable GetAll()
        {
            return con.getAllAnswers();
        }

        public DataTable GetAllAnswersFromSpecificUser(string user_number)
        {
            return con.getAllAnswersFromSpecificUser(user_number);
        }

        public DataTable getAllAnswersForSelectedQuestion(int question_id)
        {
            return con.getAllAnswersForSelectedQuestion(question_id);
        }

        public int save()
        {
            return con.SaveAnswer(this);
        }

        public string getQuestionOwnerEmail(string id)
        {

            DataTable dt = new DataTable();
            dt = con.GetDataFromDB("Select u.email from users u, question q where q.question_id = "+id+" and q.user_number = u.user_number;");

            return dt.Rows[0][0].ToString();
        }

        public static implicit operator List<object>(Answer v)
        {
            throw new NotImplementedException();
        }
    }
}