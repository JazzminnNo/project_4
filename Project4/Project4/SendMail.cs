﻿using System;
using System.Net;
using System.Net.Mail;

namespace Project4
{
    class SendMail
    {
        public string sendActivationmail(string activation_code,string email)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("non-reply@project4.com");
            msg.To.Add(email);
            msg.Subject = "Welkom bij de Q&A-app voor Informaticastudenten";
            msg.Body = "Welkom bij de Q&A-app voor Informaticastudenten -en docenten van de Hogeschool Rotterdam! " +
                "\nGebruik de onderstaande link om het zojuist geregistreerde account te activeren:" +
                "\n http://145.24.222.167/qandaccountactivation.aspx?option=accountactivation&code=" + activation_code+"\nVeel plezier gewenst en wij hopen dat de app zeer van dienst zal zijn." +
                "\nMet vriendelijke groet,\nHet Q&A - team";
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("vraagenantwoord.hr@gmail.com", "Estrela0");           
            client.Timeout = 20000;

            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Fail Has error" + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }

        public string sendActivity(string email)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("non-reply@project4.com");
            msg.To.Add(email);
            msg.Subject = "Er is een antwoord op je vraag";
            msg.Body = "Iemand heeft je vraag beantwoord!\n\n\nMet vriendelijke groet,\nHet Q&A - team";
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("vraagenantwoord.hr@gmail.com", "Estrela0");
            client.Timeout = 20000;

            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Fail Has error" + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }

        public string sendPassReset(string resetCode, string email)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("non-reply@project4.com");
            msg.To.Add(email);
            msg.Subject = "Welkom bij de Q&A-app voor Informaticastudenten";
            msg.Body = "Er is aangegeven dat het wachtwoord voor dit geregistreerde account hersteld moet worden.\nMaak gebruik van onderstaande link om een nieuw wachtwoord voor dit account in te vullen:\n" +
                "\n http://145.24.222.167/passwordreset.aspx?option=resetpassword&code=" + resetCode + "\nWaarschuwing: mocht dit verzoek bij u niet bekend zijn dan heeft een ander persoon een aanvraag gedaan voor dit geregistreerde account. In dat geval kan dit bericht genegeerd worden." +
                "\nMet vriendelijke groet,\nHet Q&A - team";
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("vraagenantwoord.hr@gmail.com", "Estrela0");
            client.Timeout = 20000;

            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Fail Has error" + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }
    }
}