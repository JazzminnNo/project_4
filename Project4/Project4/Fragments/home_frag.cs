﻿using System;
using System.Data;
using Android.OS;
using Android.Views;
using Project4.Adapters;

namespace Project4.Resources.Fragments
{
    class home_frag2 : base_frag
    {
        private int position;
        protected bool nieuwFilter;
        
        public static home_frag2 NewInstance(int position)
        {
            var f = new home_frag2();
            var b = new Bundle();

            b.PutInt("position", position);
            f.Arguments = b;
            return f;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            position = Arguments.GetInt("position");
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            questions.Clear();

            nieuwFilter = this.Activity.Intent.Extras.GetBoolean("boolFilter");
            Console.WriteLine("FROM HOME_FRAG: NIEUWFILTER IS: " + nieuwFilter);

            dt = q.getAll(nieuwFilter);

            foreach (DataRow row in dt.Rows)
            {
                Question added_question = new Question();
                added_question.id = row[0].ToString();
                added_question.title = row[1].ToString();
                added_question.question_text = row[2].ToString();
                added_question.category = row[3].ToString();
                added_question.user = row[4].ToString() + " " + row[5].ToString();
                added_question.date = added_question.GetPrettyDate(DateTime.Parse(row[6].ToString()));
                added_question.user_number = row[7].ToString();
                Console.WriteLine("FROM HOME_FRAG: ITEM.USERNUMBER = " + added_question.user_number);
                questions.Add(added_question);
            }

            FindViews();
            HandleEvents();          
            listView.Adapter = new QuestionListAdapter(this.Activity, questions);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.home_tab, container, false);
        }
    }
}