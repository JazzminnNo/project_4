﻿using System;
using System.Collections.Generic;
using System.Data;
using Android.Content;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;

namespace Project4.Resources.Fragments
{
    class base_frag : Fragment
    {
        protected DataTable dt;
        protected ListView listView;
        protected Question q = new Question();
        protected List<Question> questions = new List<Question>();
        
        string userNumber;

        public base_frag()
        {
            dt = new DataTable();
        }

        protected void FindViews()
        {
            listView = this.View.FindViewById<ListView>(Resource.Id.homeListview);
        }

        protected void HandleEvents()
        {
            listView.ItemClick += ListView_ItemClick;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var question = questions[e.Position];

            userNumber = this.Activity.Intent.Extras.GetString("usernumber");
            Console.WriteLine("PLIESSSS LAAAAAT MIJ WAAT ZIEN NU IK ZIT VAST IN BASEFRAG" + userNumber);

            var intent = new Intent(this.Activity, typeof(QuestionAnswerOverviewActivity));

            intent.PutExtra("selectedQuestionId", Int32.Parse(question.id));
            intent.PutExtra("activeUsernumber", userNumber);
            StartActivity(intent);
            this.Activity.Finish();
        }
    }
}
