package md5b8805f2e0157e77cb4b65e58b677b0ba;


public class dialog_CreateQuestion
	extends android.app.DialogFragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreateView:(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;:GetOnCreateView_Landroid_view_LayoutInflater_Landroid_view_ViewGroup_Landroid_os_Bundle_Handler\n" +
			"n_onActivityCreated:(Landroid/os/Bundle;)V:GetOnActivityCreated_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Project4.dialog_CreateQuestion, Project4", dialog_CreateQuestion.class, __md_methods);
	}


	public dialog_CreateQuestion ()
	{
		super ();
		if (getClass () == dialog_CreateQuestion.class)
			mono.android.TypeManager.Activate ("Project4.dialog_CreateQuestion, Project4", "", this, new java.lang.Object[] {  });
	}

	public dialog_CreateQuestion (java.lang.String p0)
	{
		super ();
		if (getClass () == dialog_CreateQuestion.class)
			mono.android.TypeManager.Activate ("Project4.dialog_CreateQuestion, Project4", "System.String, mscorlib", this, new java.lang.Object[] { p0 });
	}


	public android.view.View onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2)
	{
		return n_onCreateView (p0, p1, p2);
	}

	private native android.view.View n_onCreateView (android.view.LayoutInflater p0, android.view.ViewGroup p1, android.os.Bundle p2);


	public void onActivityCreated (android.os.Bundle p0)
	{
		n_onActivityCreated (p0);
	}

	private native void n_onActivityCreated (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
