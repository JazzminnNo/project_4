package md52dfc05f9f1735e415c624e8084d53e37;


public class AnswerFocusAdapterViewHolder
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Project4.Adapters.AnswerFocusAdapterViewHolder, Project4", AnswerFocusAdapterViewHolder.class, __md_methods);
	}


	public AnswerFocusAdapterViewHolder ()
	{
		super ();
		if (getClass () == AnswerFocusAdapterViewHolder.class)
			mono.android.TypeManager.Activate ("Project4.Adapters.AnswerFocusAdapterViewHolder, Project4", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
